package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;

public class BriansBrain implements CellAutomaton{

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }



    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                } else {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
            }
        }
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row, column);
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
        for (int row = 0; row < nextGeneration.numRows(); row++) {
            for (int column = 0; column < nextGeneration.numColumns(); column++) {
                currentGeneration.set(row, column, getNextCell(row, column));
            }
        }
    }

    @Override
    public CellState getNextCell(int row, int col) {
        int aliveNeighbors = countNeighbors(row, col);
        if (getCellState(row, col) == CellState.ALIVE) {
            return CellState.DYING;
        }
        else if (getCellState(row, col) == CellState.DYING) {
            return CellState.DEAD;
        }
        else {
            if (aliveNeighbors == 2) {
                return CellState.ALIVE;
            }
            else {
                return CellState.DEAD;
            }
        }
    }

    private int countNeighbors(int row, int col) {
        int[] xOffset = {-1, -1, -1, 0, 0, 1, 1, 1};
        int[] yOffset = {-1, 0, 1, -1, 1, -1, 0, 1};
        int count = 0;
        for (int i = 0; i < 8; i++) {
            int neighborX = row + xOffset[i];
            int neighborY = col + yOffset[i];
            if (neighborX < 0 || neighborX >= currentGeneration.numRows() ||
                    neighborY < 0 || neighborY >= currentGeneration.numColumns()) {
                continue;
            }
            if (currentGeneration.get(neighborX, neighborY) == CellState.ALIVE) {
                count++;
            }
        }
        return count;
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
}
