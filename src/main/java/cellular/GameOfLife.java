package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * A CellAutomata that implements Conways game of life.
 *
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 * @see CellAutomaton
 * <p>
 * Every cell has two states: Alive or Dead. Each step the state of each
 * cell is decided from its neighbors (diagonal, horizontal and lateral).
 * If the cell has less than two alive Neighbors or more than three
 * neighbors the cell dies. If a dead cell has exactly three neighbors it
 * will become alive.
 */
public class GameOfLife implements CellAutomaton {

    /**
     * The grid of cells
     */
    IGrid currentGeneration;

    /**
     * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
     * provided size
     *
     * param height The height of the grid of cells
     * param width  The width of the grid of cells
     */

    public GameOfLife(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                } else {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
            }
        }
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public CellState getCellState(int row, int col) {
        return currentGeneration.get(row, col);
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
        for (int row = 0; row < nextGeneration.numRows(); row++) {
            for (int column = 0; column < nextGeneration.numColumns(); column++) {
                currentGeneration.set(row, column, getNextCell(row, column));
            }
        }
    }

    @Override
    public CellState getNextCell(int row, int col) {
        int aliveNeighbors = countNeighbors(row, col);
        if (getCellState(row, col) == CellState.ALIVE) {
            if (aliveNeighbors < 2 || aliveNeighbors > 3) {
                return CellState.DEAD;
            }
            return CellState.ALIVE;
        }
        else {
            if (aliveNeighbors == 3) {
                return CellState.ALIVE;
            }
            return CellState.DEAD;
        }
    }

    /**
     * Calculates the number of neighbors having a given CellState of a cell on
     * position (row, col) on the board
     * <p>
     * Note that a cell has 8 neighbors in total, of which any number between 0 and
     * 8 can be the given CellState. The exception are cells along the boarders of
     * the board: these cells have anywhere between 3 neighbors (in the case of a
     * corner-cell) and 5 neighbors in total.
     *
     * param x     the x-position of the cell
     * param y     the y-position of the cell
     * param *state the Cell-state we want to count occurrences of.
     * @return the number of neighbors with given state
     *
     *
     * I'm using a nice algorithm for locating neighbors of a cell in a 2D array. This works by making
     * two lists containing the x-offset and the y-offset for the working celle. Then we are looping
     * through a for loop with as many steps as desired neighbors (length of the offsets lists). For each
     * step we generate a coordinate adding position "i" at x-list with the working cell's x-position,
     * and then adding position "i" at y-list with the working cell's y-position. We continue by checking
     * if the new coordinate is "out of bounds" of the grid. if so, we disregard this coordinate and
     * continue to the next neighbour. If the neighbour is not outside the grid, we check if it is alive,
     * and if so, we add 1 to the neighbour count.
     *
     *        3x3                 offset
     *  (0,0)(0,1)(0,2)     (-1,-1) (-1,0) (-1,1)
     *  (1,0)(1,1)(1,2)     (0,-1)         (0,1)
     *  (2,0)(2,1)(2,2)     (1,-1)  (1,0)  (1,1)
     */

    private int countNeighbors(int row, int col) {
        int[] xOffset = {-1, -1, -1, 0, 0, 1, 1, 1};
        int[] yOffset = {-1, 0, 1, -1, 1, -1, 0, 1};
        int count = 0;
        for (int i = 0; i < 8; i++) {
            int neighborX = row + xOffset[i];
            int neighborY = col + yOffset[i];
            if (neighborX < 0 || neighborX >= currentGeneration.numRows() ||
                    neighborY < 0 || neighborY >= currentGeneration.numColumns()) {
                continue;
            }
            if (currentGeneration.get(neighborX, neighborY) == CellState.ALIVE) {
                count++;
            }
        }
        return count;
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
}
